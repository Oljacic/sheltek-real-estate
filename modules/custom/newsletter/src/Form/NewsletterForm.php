<?php
/**
 * Created by PhpStorm.
 * User: soul
 * Date: 23.11.17.
 * Time: 22.38
 */

namespace Drupal\newsletter\Form;

use Drupal\Core\Database\Database;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class NewsletterForm extends FormBase
{

    public function getFormId()
    {
        return 'newsletter_form';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form = [
            '#markup' => '<div class="wrap-title-newsleatter"><h3>Subscribe</h3><h2>Newsletter</h2></div>',
        ];

        $form['#prefix'] = '<div class="wrap-input-newsletter">';

        $form['email'] = array(
            '#prefix' => '<div class="wrap">',
            '#type' => 'email',
            '#size' => 25,
            '#placeholder' => t('Enter your email here...'),
        );

        $form['actions']['#type'] = 'actions';
        $form['actions']['submit'] = array(
            '#type' => 'submit',
            '#value' => t('Send'),
            '#button_type' => 'primary',
            '#suffix' => '</div>',
        );

        $form['#suffix'] = '</div>';
        return $form;
    }

    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        $value = $form_state->getValue('email');
        if ($value == !\Drupal::service('email.validator')->isValid($value)) {
            $form_state->setErrorByName('email', t('The email address %mail is not valid.', array('%mail' => $value)));
        }
    }

    public function submitForm(array &$form, FormStateInterface $form_state)
    {

        $values = $form_state->getValue('email');
        $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());

        $email = \Drupal::database()->select('newsletter', 'n');
        $email->condition('n.mail', $values, '=');
        $email->addField('n', 'mail');
        $emails = $email->execute()->fetchAll();

        if (empty($emails)) {
            \Drupal::database()->insert('newsletter')
                ->fields(array(
                    'mail' => $form_state->getValue('email'),
                    'nid' => $form_state->getValue('nid'),
                    'uid' => $user->id(),
                    'created' => time(),
                ))
                ->execute();
            drupal_set_message(t('Thank for you subscribe, you are on our list for newsletter.'));
        } else {
            drupal_set_message(t('Email exist. Please type new address!'), $type = 'warning');
        }


    }


}